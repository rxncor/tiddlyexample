# An Example Tiddly Wiki

Now based on node.js!

## Hosted at:
   https://bitbucket.org/rxncor/tiddlyexample

## How to start it

Make sure you have node.js installed (see below)

### Get the notes for the first time:
   `cd ~ ; git clone https://bitbucket.org/rxncor/tiddlyexample`

### Make sure you have the latest notes :
   `git fetch ; git diff ; git merge` or `git pull`. git fetch is safer!

### Start node.js based tiddlywiki
  - assume notes are in ~/exampletiddly
  - `cd ~; tiddlywiki exampletiddly --server`
  - browse to localhost:8080

## How to install Tiddly on Node.js

# Windows
   goto nodejs website and download. install. open node cmd window, npm install tiddlywiki. 
# Linux
   As root or sudo... `yum --enablerepo=epel install nodejs npm -y; npm install tiddlywiki`

## No need to use firefox or tiddly fox anymore
but if it weren't for node
Remember to save each tiddler and download. 
USE TiddlyFox.

